function createNewUser() {
    let newUser = {
        getLogin() { return (this.firstName.substring(0, 1) + this.lastName).toLowerCase(); },
        setFirstName(firstName) {
            Object.defineProperty(this, "firstName", {
                value: firstName,
            });
        },
        setLastName(lastName) {
            Object.defineProperty(this, "lastName", {
                value: lastName,
            });
        }
    };

    Object.defineProperty(newUser, "firstName", {
        value: prompt("Введіть своє ім'я будь-ласка."),
        writable: false,
    });

    Object.defineProperty(newUser, "lastName", {
        value: prompt("Введіть своє прізвище."),
        writable: false,
    });
    return newUser;
};

let myUser = createNewUser();
console.log(myUser.getLogin());

