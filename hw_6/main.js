function createNewUser() {
    let newUser = {
        getLogin() {
            return (this.firstName.substring(0, 1) + this.lastName).toLowerCase();
        },
        getAge() {
            let age = new Date().getFullYear() - new Date(birthday.slice(-4), (birthday.slice(3, 5) - 1), birthday.slice(0, 1)).getFullYear();
            let month = new Date().getMonth() - (birthday.slice(3, 5) - 1);
            if (month < 0 || (month === 0 && new Date().getDate() < birthday.slice(0, 2))) {
                age++;
            };
            return age;
        },
        getPassword() {
            return this.firstName.substring(0, 1).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);
        },
        setFirstName(newFirstName) {
            Object.defineProperty(this, "firstName", {
                value: newFirstName,
            });
        },
        setLastName(newLastName) {
            Object.defineProperty(this, "lastName", {
                value: newLastName,
            });
        },
        setBirthday(newBirthday) {
            Object.defineProperty(this, "birthday", {
                value: newBirthday,
            });
        },
    };

    Object.defineProperty(newUser, "firstName", {
        value: prompt("Введіть будь-ласке ваше ім'я."),
    });

    Object.defineProperty(newUser, "lastName", {
        value: prompt("Введіть будь-ласка ваше прізвище."),
    });
    
    let day, month, year;

    while (!day || isNaN(day) || day > 31 || day.length > 2) {
        day = prompt("Введіть день вашого народження. У проміжку від 1 до 31.");
        if (day < 10) day = "0" + day;
    };
    while (!month || isNaN(month) || month > 12 || month.length > 2) {
        month = prompt("Введіть місяць свого народження. У проміжку від 01 до 12.");
        if (month < 10) month = `${month}`;
        switch (month) {
            case 1:
                while (!day || isNaN(day) || day > 29 || day.length > 2) {
                    day = prompt("Введіть день свого народження. Чотирьох-значне.");
                    if (day < 10) day = "0" + day;
                };
                break;
            case 2:
                while (!day || isNaN(day) || day > 30 || day.length > 2) {
                    day = prompt("Введіть день свого народження. Чотирьох-значне.");
                    if (day < 10) day = "0" + day;
                };
                break;
            default:
                break;
        };
    };
    while (!year || isNaN(year) || new Date().getFullYear() < year || year < 1900 || year > 2050 || year.length !== 4) {
        year = prompt("Введіть рік свого народження. Чотирьох-значне число.");
        if (new Date(year, 1, 29).getMonth() != 1) {
            day = prompt("Введіть день народження користувача. Двоцифрофе число від 1 до 31.");
            while (!day || isNaN(day) || day > 31 || day.length > 2) {
                day = prompt("Введіть день народження користувача. Двоцифрофе число від 1 до 31.");
                if (day < 10) day = "0" + day;
            };
        };
    };

    let birthday = (`${day}.${month}.${year}`);
    Object.defineProperty(newUser, "birthday", {
        value: birthday,
    });
    return newUser;
};

let myUser = createNewUser();
console.log(myUser);
console.log(myUser.getLogin());
console.log(myUser.getAge());
console.log(myUser.getPassword());
