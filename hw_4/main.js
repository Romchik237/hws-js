let Num1 = prompt("Введіть перше число будь-ласка: ");
while (isNaN(Num1) || Num1 === "") {
    alert("Будь-ласка введіть дійсне перше число.");
    Num1 = prompt("Введіть перше число: ");
};
console.log (`Ви ввели: ${Num1}`);

let Num2 = prompt("Введіть друге число: ");
while (isNaN(Num2) || Num2 === "") {
    alert("Будь-ласка введіть дійсне друге число.");
    Num2 = prompt ("Введіть друге число: ");
};
console.log (`Ви ввели: ${Num2}`);

let op = prompt("Введіть математичну операцію між числами: ");
while (op != "+" && op != "-" && op != "*" && op != "/") {
    alert("Будь-ласка введіть дійсну математичну операцію між числами.");
    op = prompt("Введіть одну із чотирьох операцій: ");
};
console.log (`Ви ввели операцію: ${op}`);

calc (Num1, Num2, op);
function calc (a, b, op) {
    let result;
    
    switch (op) {
        case "+":
            result = +a + +b;
            break;
        case "-":
            result = a - b;
            break;
        case "*":
            result = a * b;
            break;
        case "/":
            result = a / b;
            break;
        default:
            break;
    };
    console.log(`Відповідь: ${result}`);
};
