let name = "Roman";
let admin = name;
console.log (admin);


let daysInWeek = 7;
let secondsInMinute = 60;
let minutesInHour = 60;
let hoursInDay = 24;
let daysInSecond = daysInWeek * (60 * 60 * 24);
console.log(daysInSecond);


let nameNew = prompt ("Введіть своє ім'я ");
let age = prompt ("Введіть свій вік ");
console.log (`Привіт ${nameNew}, я бачу що тобі ${age} років, тому ти підходиш.`);

// 1) Можна оголошувати змінну через ключові слова let, const та var. Але змінювати значення змінної можна тільки у змінній let.
// 2) Функція confirm показує модальне вікно з питанням question та двома кнопками: ОК та Скасувати.
// Функція prompt викликає також модальне вікно з текстовим повідомленням, 
// полем, куди відвідувач може ввести текст, та кнопками ОК/Скасувати. І виводить повне повідомлення, в залежності від поставленого запитання.
// А у функції confirm виводить true чи false після відповіді користувача без повної відповіді. 
// 3) Неявне перетворення типів, також відоме як примусове, це автоматичне перетворення типів при компіляції.
// Оскільки JavaScript є мовою зі слабкою типізацією, значення можуть бути конвертовані 
// між різними типами автоматично. Це називають неявним наведенням типів. Зазвичай таке відбувається, 
// як у висловлюваннях використовують значення різних типів, на зразок 1 == null, 
// 2/’5', null + new Date(). Неявне перетворення типів може бути викликано і 
// контекстом виразу, на зразок if (value) {…} де value неявно приводиться до логічного типу даних.
// Існує оператор, який викликає неявного перетворення типів — це оператор суворої рівності, ===. 
// Неявне перетворення типів - палиця з двома кінцями: це джерело плутанини та помилок, 
// але це і корисний механізм, який дозволяє писати менше коду без втрати його читабельності.




























